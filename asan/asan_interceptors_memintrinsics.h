#ifndef ASAN_INTERCEPTORS_MEMINTRINSICS_h
#define ASAN_INTERCEPTORS_MEMINTRINSICS_h

#include "asan_interceptors.h"
#include "asan_internal_defs.h"
#include "asan_internal.h"
#include "asan_mapping.h"
#include "asan_alloctor.h"

using namespace __sanitizer;

// DECLARE_REAL(void*, memcpy, void *to, const void *from, uptr size)
// DECLARE_REAL(void*, memset, void *block, int c, uptr size)
// DECLARE_REAL(void*, memmove, void* to, const void* from, uptr size)

extern "C" {

// Those functions could be also invoked during __asan_init
// Before or during __asan_init, we invoke REAL(XXX) only
// After the __asan_init, we also check the shadow
void* __asan_memcpy(void* to, const void* from, uptr size);
void* __asan_memset(void* block, int c, uptr size);
void* __asan_memmove(void* to, const void* from, uptr size);

}


# endif // ASAN_INTERCEPTORS_MEMINTRINSICS_h
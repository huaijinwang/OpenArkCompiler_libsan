#ifndef ASAN_SIGNAL_HANDLER_H
#define ASAN_SIGNAL_HANDLER_H
#include "asan_internal_defs.h"
#include "asan_internal.h"
#include "asan_flags.h"
#include <signal.h>

namespace __sanitizer {

inline const char* GetSignalStr(int signal) {
  switch (signal)
  {
    case SIGFPE:
      return "FPE";
    case SIGILL:
      return "ILL";
    case SIGABRT:
      return "ABRT";
    case SIGSEGV:
      return "SEGV";
    case SIGBUS:
      return "BUS";
    case SIGTRAP:
      return "TRAP";
  default:
    return "UNKNOWN SIGNAL";
  }
}

struct SignalContext {
  siginfo_t *siginfo;
  ucontext_t *context;
  uptr addr;
  bool is_memory_access;
  enum WriteFlag { Unknown, Read, Write } write_flag;
  bool is_true_faulting_addr;

  SignalContext(siginfo_t* siginfo, ucontext_t* context);
};

typedef void (*SignalHandlerFunc)(int, siginfo_t *, void *);

void DeadlySignalHandler(int signal, siginfo_t* info, void* context);

void InstallDeadlySignalHandler(SignalHandlerFunc);

} // namespace __sanitizer

#endif // ASAN_SIGNAL_HANDLER_H
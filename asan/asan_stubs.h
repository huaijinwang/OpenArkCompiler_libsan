#ifndef ASAN_STUBS_H
#define ASAN_STUBS_H

#include "asan_internal_defs.h"

namespace __sanitizer {

extern "C" {

INTERFACE_ATTRIBUTE void __asan_report_load4(__sanitizer::uptr x);

}
} // namespace __sanitizer



#endif // ASAN_STUBS_H
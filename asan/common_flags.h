#ifndef SANITIZER_COMMON_FLAGS_H
#define SANITIZER_COMMON_FLAGS_H

#include "asan_internal_defs.h"


namespace __sanitizer {

enum HandleSignalMode {
  kHandleSignalNo,
  kHandleSignalYes,
  kHandleSignalExclusive,
};


class CommonFlags {
public:
#define COMMON_FLAG(Type, Name, DefaultValue, Description) Type Name;
#include "sanitizer_flags.inc"
#undef COMMON_FLAGS
  void SetDefaults();
  void CopyFrom(const CommonFlags &other);
};


// Functions to get/set global CommonFlags shared by all sanitizer runtimes:
extern CommonFlags common_flags_dont_use;
inline const CommonFlags *common_flags() {
  return &common_flags_dont_use;
}

inline void SetCommonFlagsDefaults() {
  common_flags_dont_use.SetDefaults();
}

}

#endif // SANITIZER_COMMON_FLAGS_H
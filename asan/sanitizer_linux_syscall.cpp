/*
This is a copy of LLVM-ASAN's internal syscall's implementation for aarch64
I am not sure if I need to use it.
*/
#include "asan_internal_defs.h"

#include "sanitizer_linux_syscall.h"
// platform relative
#include "sanitizer_linux_syscall_NR_aarch64.h"

namespace __sanitizer {

uptr internal_openat(int dirfd, const char *pathname, int flags, int mode) {
  return syscall(SYSCALL(openat), dirfd, pathname, flags, mode);
}

uptr internal_ftruncate(int fd, OFF_T length) {
  return syscall(SYSCALL(ftruncate), fd, length);
}

uptr internal_mmap(void *addr, uptr length, int prot, int flags, int fd, u64 offset) {
  return syscall(SYSCALL(mmap), (uptr)addr, length, prot, flags, fd, offset);
}

uptr internal_unlink(const char* path) {
  return syscall(SYSCALL(unlinkat), AT_FDCWD, (uptr)path, 0);
}

} // namespace __sanitizer
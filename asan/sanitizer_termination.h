#ifndef SANITIZER_TERMINATION_H
#define SANITIZER_TERMINATION_H

#include "asan_internal_defs.h"


namespace __sanitizer{

typedef void (*DieCallbackType)(void);

bool AddDieCallback(DieCallbackType callback);
bool RemoveDieCallback(DieCallbackType callback);
void NORETURN Die();

} // namespace __sanitizer


#endif // SANITIZER_TERMINATION_H
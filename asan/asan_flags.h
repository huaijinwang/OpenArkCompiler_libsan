#ifndef SANITIZER_FLAGS_H
#define SANITIZER_FLAGS_H
#include "common_flags.h"


namespace __sanitizer
{

struct AsanFlags {
#define ASAN_FLAG(Type, Name, DefaultValue, Description) Type Name;
#include "asan_flags.inc"
#undef ASAN_FLAG

  void SetDefaults();
};


extern AsanFlags asan_flags_dont_use_directly;
inline AsanFlags *asan_flags() {
  return &asan_flags_dont_use_directly;
}

void InitializeAsanFlags();

} // namespace __sanitizer


#endif // SANITIZER_FLAGS_H
#include "asan_mapping.h"

namespace __sanitizer {

uptr ASAN_SHADOW_BEG = -1;
uptr ASAN_SHADOW_END = 0; // to be initialized
uptr ASAN_TOTAL_PHYS_PAGES, ASAN_PAGE_SIZE;

} // namespace __sanitizer

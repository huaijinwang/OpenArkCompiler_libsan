#ifndef ASAN_INTERNAL_H
#define ASAN_INTERNAL_H

#include "asan_internal_defs.h"
#include "asan_shadow_memory.h"


namespace __sanitizer {

// Magic values for reporting, the same as LLVM-ASAN
const su_t kAsanHeapLeftRedzoneMagic = 0xfa;
const su_t kAsanHeapFreeMagic = 0xfd;
const su_t kAsanStackLeftRedzoneMagic = 0xf1;
const su_t kAsanStackMidRedzoneMagic = 0xf2;
const su_t kAsanStackRightRedzoneMagic = 0xf3;
const su_t kAsanStackAfterReturnMagic = 0xf5;
const su_t kAsanInitializationOrderMagic = 0xf6;
const su_t kAsanUserPoisonedMemoryMagic = 0xf7;
const su_t kAsanContiguousContainerOOBMagic = 0xfc;
const su_t kAsanStackUseAfterScopeMagic = 0xf8;
const su_t kAsanGlobalRedzoneMagic = 0xf9;
const su_t kAsanInternalHeapMagic = 0xfe;
const su_t kAsanArrayCookieMagic = 0xac;
const su_t kAsanIntraObjectRedzone = 0xbb;
const su_t kAsanAllocaLeftMagic = 0xca;
const su_t kAsanAllocaRightMagic = 0xcb;

static const uptr kCurrentStackFrameMagic = 0x41B58AB3;
static const uptr kRetiredStackFrameMagic = 0x45E0360E;
} // namespace __sanitizer


#endif // ASAN_INTERNAL_H
#ifndef SANITIZER_LIBC_H
#define SANITIZER_LIBC_H

#include "asan_internal_defs.h"
#include "wchar.h"

namespace __sanitizer {

extern "C" {

INTERFACE_ATTRIBUTE void* internal_malloc(uptr size);
INTERFACE_ATTRIBUTE void* internal_calloc(uptr num, uptr size);
INTERFACE_ATTRIBUTE void* internal_realloc(void* addr, uptr size);
INTERFACE_ATTRIBUTE s64 internal_atoll(const char *nptr);
INTERFACE_ATTRIBUTE char* internal_itoa(int value, char* str, int base);
INTERFACE_ATTRIBUTE char* internal_lltoa(long long value, char* str, int base);
INTERFACE_ATTRIBUTE void *internal_memchr(const void *s, int c, uptr n);
INTERFACE_ATTRIBUTE void *internal_memrchr(const void *s, int c, uptr n);
INTERFACE_ATTRIBUTE int internal_memcmp(const void* s1, const void* s2, uptr n);
INTERFACE_ATTRIBUTE void *internal_memcpy(void *dest, const void *src, uptr n);
INTERFACE_ATTRIBUTE void *internal_memmove(void *dest, const void *src, uptr n);
INTERFACE_ATTRIBUTE void *internal_memset(void* s, int c, uptr n);
INTERFACE_ATTRIBUTE uptr internal_strcspn(const char *s, const char *reject);
INTERFACE_ATTRIBUTE char* internal_strdup(const char *s);
INTERFACE_ATTRIBUTE int internal_strcmp(const char *s1, const char *s2);
INTERFACE_ATTRIBUTE int internal_strncmp(const char *s1, const char *s2, uptr n);
INTERFACE_ATTRIBUTE char* internal_strchr(const char *s, int c);
INTERFACE_ATTRIBUTE char *internal_strchrnul(const char *s, int c);
INTERFACE_ATTRIBUTE char *internal_strrchr(const char *s, int c);
INTERFACE_ATTRIBUTE uptr internal_strlen(const char *s);
INTERFACE_ATTRIBUTE uptr internal_strlcat(char *dst, const char *src, uptr maxlen);
INTERFACE_ATTRIBUTE char *internal_strncat(char *dst, const char *src, uptr n);
INTERFACE_ATTRIBUTE uptr internal_strlcpy(char *dst, const char *src, uptr maxlen);
INTERFACE_ATTRIBUTE char *internal_strncpy(char *dst, const char *src, uptr n);
INTERFACE_ATTRIBUTE uptr internal_strnlen(const char *s, uptr maxlen);
INTERFACE_ATTRIBUTE char *internal_strstr(const char *haystack, const char *needle);
INTERFACE_ATTRIBUTE s64 internal_simple_strtoll(const char *nptr, const char **endptr, int base);
INTERFACE_ATTRIBUTE uptr internal_wcslen(const wchar_t *s);
INTERFACE_ATTRIBUTE uptr internal_wcsnlen(const wchar_t *s, uptr maxlen);
INTERFACE_ATTRIBUTE bool mem_is_zero(const char *beg, uptr size);

}

} // namespace __sanitizer

#endif // SANITIZER_LIBC_H

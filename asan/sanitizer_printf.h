#ifndef SANITIZER_PRINTF_H
#define SANITIZER_PRINTF_H
#include "stdio.h"
#include "asan_internal_defs.h"
#include "common_flags.h"
#include "asan_internal.h"
#include <stdio.h>

#define Report Printf

namespace __sanitizer {

bool ColorizeReports();

class PrintfDecorator {
 public:
  PrintfDecorator() : ansi_(ColorizeReports()) {}
  inline const char *Bold() const { return ansi_ ? "\033[1m" : ""; }
  inline const char *Default() const { return ansi_ ? "\033[1m\033[0m"  : ""; }
  inline const char *Warning() const { return Red(); }
  inline const char *Error() const { return Red(); }
  inline const char *MemoryByte() const { return Magenta(); }
  inline const char *Black()   const { return ansi_ ? "\033[1m\033[30m" : ""; }
  inline const char *Red()     const { return ansi_ ? "\033[1m\033[31m" : ""; }
  inline const char *Green()   const { return ansi_ ? "\033[1m\033[32m" : ""; }
  inline const char *Yellow()  const { return ansi_ ? "\033[1m\033[33m" : ""; }
  inline const char *Blue()    const { return ansi_ ? "\033[1m\033[34m" : ""; }
  inline const char *Magenta() const { return ansi_ ? "\033[1m\033[35m" : ""; }
  inline const char *Cyan()    const { return ansi_ ? "\033[1m\033[36m" : ""; }
  inline const char *White()   const { return ansi_ ? "\033[1m\033[37m" : ""; }
  const char* GetShadowByteStr(su_t v);
 private:
  bool ansi_;
  char shadow_byte_buff[32];
};

void CheckPrintfVars(const char* format, va_list copy_args);

} // namespace __sanitizer

#endif // SANITIZER_PRINTF_H
#ifndef ASAN_POISONING_H
#define ASAN_POISONING_H
#include "asan_internal_defs.h"

namespace __sanitizer {
extern "C" {

void __asan_set_shadow_00(uptr addr, uptr size);

void __asan_set_shadow_f1(uptr addr, uptr size);

void __asan_set_shadow_f2(uptr addr, uptr size);

void __asan_set_shadow_f3(uptr addr, uptr size);

void __asan_set_shadow_f5(uptr addr, uptr size);

void __asan_set_shadow_f8(uptr addr, uptr size);
}

}  // namespace __sanitizer

#endif  // ASAN_POISONING_H
#define SYSCALL(name) __NR_ ## name

#define __NR_ftruncate 46
#define __NR_openat 56
#define __NR_mmap 222

#include "common_flags.h"
#include "asan_flags.h"


namespace __sanitizer
{

AsanFlags asan_flags_dont_use_directly;

void AsanFlags::SetDefaults() {
#define ASAN_FLAG(Type, Name, DefaultValue, Description) Name = DefaultValue;
#include "asan_flags.inc"
#undef ASAN_FLAG
}

void InitializeAsanFlags() {
  // Google use a register mechanism, not sure if it is necessary.
  // Currently, we read from those flags directly anyway
  asan_flags_dont_use_directly.SetDefaults();
}

} // namespace __sanitizer


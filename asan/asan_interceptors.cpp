#include <cstddef>
#include <cstdlib>
#include <cstdio>
#include <wchar.h>
#include <pthread.h>
#include "asan_internal_defs.h"
#include "asan_interceptors.h"
#include "interception.h"

#define ASAN_INTERFACES_MACRO DECLARE_REAL_AND_INTERCEPTOR
#include "asan_interfaces.inc"
#undef ASAN_INTERFACES_MACRO

using namespace __sanitizer;


extern "C" {
void InitializeAsanInterceptors() {
  static bool was_called_once = false;
  ASSERT(!was_called_once, "__sanitizer::InitializeAsanInterceptors was called again");
  was_called_once = true;

  ASAN_INTERCEPT_FUNC(malloc);
  ASAN_INTERCEPT_FUNC(calloc);
  ASAN_INTERCEPT_FUNC(realloc);
  ASAN_INTERCEPT_FUNC(free);
  // ASAN_INTERCEPT_FUNC(alloca);

  ASAN_INTERCEPT_FUNC(memcpy);
  ASAN_INTERCEPT_FUNC(memset);
  ASAN_INTERCEPT_FUNC(memmove);

  // InitializeCommonInterceptors();
  // InitializeSignalInterceptors();

  // Intercept str* functions.
  ASAN_INTERCEPT_FUNC(memcmp);
  ASAN_INTERCEPT_FUNC(strlen);
  ASAN_INTERCEPT_FUNC(strnlen);
  ASAN_INTERCEPT_FUNC(strcat);
  ASAN_INTERCEPT_FUNC(strncat);
  ASAN_INTERCEPT_FUNC(strcpy);
  ASAN_INTERCEPT_FUNC(strncpy);
  ASAN_INTERCEPT_FUNC(strdup);

  ASAN_INTERCEPT_FUNC(wcscpy);
  ASAN_INTERCEPT_FUNC(wcsncpy);

  ASAN_INTERCEPT_FUNC(printf);
  ASAN_INTERCEPT_FUNC(snprintf);

  // To handle several overloaded functions
  const char* (*__asan_strchr_1)(const char *, int) = &strchr;
  ASAN_INTERCEPT_FUNC_WITH_ADDR(strchr, __asan_strchr_1);
  char* (*__asan_strchr_2)(char *, int) = &strchr;
  ASAN_INTERCEPT_FUNC_WITH_ADDR(strchr, __asan_strchr_2);

  const char* (*__asan_strstr_1)(const char *, const char *) = &strstr;
  ASAN_INTERCEPT_FUNC_WITH_ADDR(strstr, __asan_strstr_1);
  char* (*__asan_strstr_2)(char *, const char *) = &strstr;
  ASAN_INTERCEPT_FUNC_WITH_ADDR(strstr, __asan_strstr_2);
#if ASAN_INTERCEPT___STRDUP
  ASAN_INTERCEPT_FUNC(__strdup);
#endif
#if ASAN_INTERCEPT_INDEX && ASAN_USE_ALIAS_ATTRIBUTE_FOR_INDEX
  ASAN_INTERCEPT_FUNC(index);
#endif

  // ASAN_INTERCEPT_FUNC(atoi);
  // ASAN_INTERCEPT_FUNC(atol);
  // ASAN_INTERCEPT_FUNC(strtol);
#if ASAN_INTERCEPT_ATOLL_AND_STRTOLL
  ASAN_INTERCEPT_FUNC(atoll);
  ASAN_INTERCEPT_FUNC(strtoll);
#endif

  // Intecept jump-related functions.
  // ASAN_INTERCEPT_FUNC(longjmp);

#if ASAN_INTERCEPT_SWAPCONTEXT
  ASAN_INTERCEPT_FUNC(getcontext);
  ASAN_INTERCEPT_FUNC(swapcontext);
#endif
#if ASAN_INTERCEPT__LONGJMP
  ASAN_INTERCEPT_FUNC(_longjmp);
#endif
#if ASAN_INTERCEPT___LONGJMP_CHK
  ASAN_INTERCEPT_FUNC(__longjmp_chk);
#endif
#if ASAN_INTERCEPT_SIGLONGJMP
  ASAN_INTERCEPT_FUNC(siglongjmp);
#endif

  // Intercept exception handling functions.
#if ASAN_INTERCEPT___CXA_THROW
  ASAN_INTERCEPT_FUNC(__cxa_throw);
#endif
#if ASAN_INTERCEPT___CXA_RETHROW_PRIMARY_EXCEPTION
  ASAN_INTERCEPT_FUNC(__cxa_rethrow_primary_exception);
#endif
  // Indirectly intercept std::rethrow_exception.
#if ASAN_INTERCEPT__UNWIND_RAISEEXCEPTION
  INTERCEPT_FUNCTION(_Unwind_RaiseException);
#endif
  // Indirectly intercept std::rethrow_exception.
#if ASAN_INTERCEPT__UNWIND_SJLJ_RAISEEXCEPTION
  INTERCEPT_FUNCTION(_Unwind_SjLj_RaiseException);
#endif

  // Intercept threading-related functions
#if ASAN_INTERCEPT_PTHREAD_CREATE
// TODO: this should probably have an unversioned fallback for newer arches?
#if defined(ASAN_PTHREAD_CREATE_VERSION)
  ASAN_INTERCEPT_FUNC_VER(pthread_create, ASAN_PTHREAD_CREATE_VERSION);
#else
  ASAN_INTERCEPT_FUNC(pthread_create);
#endif
  ASAN_INTERCEPT_FUNC(pthread_join);
#endif

  // Intercept atexit function.
#if ASAN_INTERCEPT___CXA_ATEXIT
  ASAN_INTERCEPT_FUNC(__cxa_atexit);
#endif

#if ASAN_INTERCEPT_ATEXIT
  ASAN_INTERCEPT_FUNC(atexit);
#endif

#if ASAN_INTERCEPT_PTHREAD_ATFORK
  ASAN_INTERCEPT_FUNC(pthread_atfork);
#endif

#if ASAN_INTERCEPT_VFORK
  ASAN_INTERCEPT_FUNC(vfork);
#endif

  // VReport(1, "AddressSanitizer: libc interceptors initialized\n");
}

}
#ifndef ASAN_MAPPING_H
#define ASAN_MAPPING_H

#include "asan_internal_defs.h"

namespace __sanitizer {

#define ASAN_SHADOW_OFFSET_CONST 0x0000001000000000
#define ASAN_SHADOW_OFFSET ASAN_SHADOW_OFFSET_CONST
#define ASAN_SHADOW_SCALE 3
#define ASAN_PREMAP_SHADOW 0
#define ASAN_SHADOW_GRANULARITY (1ULL << ASAN_SHADOW_SCALE)
// #define ASAN_PAGE_SIZE 4096
extern uptr ASAN_PAGE_SIZE; // initialize while building shadow memory
extern uptr ASAN_TOTAL_PHYS_PAGES;
void InitializePageSize();

// We add an extra page before and after the
// kLowShadowBeg and KHighShadowEnd
extern uptr ASAN_SHADOW_BEG; // to be initialized
extern uptr ASAN_SHADOW_END; // to be initialized

#define MEM_TO_SHADOW(mem) (((mem) >> ASAN_SHADOW_SCALE) + (ASAN_SHADOW_OFFSET))
#define SHADOW_TO_MEM(shadow) (((shadow) - (ASAN_SHADOW_OFFSET)) << ASAN_SHADOW_SCALE)

static const u64 kDefaultShadowSentinel = ~(uptr)0;
static const u64 kConstShadowOffset = ASAN_SHADOW_OFFSET_CONST;

// Initialized in __asan_init
extern uptr kHighMemEnd, kMidMemBeg, kMidMemEnd;

// The offsets of Beg and End are reversed
#define kLowMemBeg 0
#define kLowMemEnd (ASAN_SHADOW_OFFSET ? ASAN_SHADOW_OFFSET - 1 : 0)

#define kLowShadowBeg ASAN_SHADOW_OFFSET
#define kLowShadowEnd MEM_TO_SHADOW(kLowMemEnd)

#define kHighMemBeg (MEM_TO_SHADOW(kHighMemEnd) + 1)

#define kHighShadowBeg MEM_TO_SHADOW(kHighMemBeg)
#define kHighShadowEnd MEM_TO_SHADOW(kHighMemEnd)

#define kMidShadowBeg MEM_TO_SHADOW(kMidMemBeg)
#define kMidShadowEnd MEM_TO_SHADOW(kMidMemEnd)

// With the zero shadow base we can not actually map pages starting from 0.
// This constant is somewhat arbitrary.
#define kZeroBaseShadowStart 0
#define kZeroBaseMaxShadowStart (1 << 18)

// When kLowShadowEnd = 0, we have no low shadow, the gap starts from 0
#define kShadowGapBeg (kLowShadowEnd ? kLowShadowEnd + 1 : kZeroBaseShadowStart)
#define kShadowGapEnd ((kMidMemBeg ? kMidShadowBeg : kHighShadowBeg) - 1)

#define kShadowGap2Beg (kMidMemBeg ? kMidShadowEnd + 1 : 0)
#define kShadowGap2End (kMidMemBeg ? kMidMemBeg - 1 : 0)

#define kShadowGap3Beg (kMidMemBeg ? kMidMemEnd + 1 : 0)
#define kShadowGap3End (kMidMemBeg ? kHighShadowBeg - 1 : 0)

#define DO_ASAN_MAPPING_PROFILE 0
extern uptr AsanMappingProfile[];
#  if DO_ASAN_MAPPING_PROFILE
#    define PROFILE_ASAN_MAPPING() AsanMappingProfile[__LINE__]++;
#  else
#    define PROFILE_ASAN_MAPPING()
#  endif

static inline bool AddrIsInLowMem(uptr a) {
  PROFILE_ASAN_MAPPING();
  return a <= kLowMemEnd;
}

static inline bool AddrIsInLowShadow(uptr a) {
  PROFILE_ASAN_MAPPING();
  return a >= kLowShadowBeg && a <= kLowShadowEnd;
}

static inline bool AddrIsInMidMem(uptr a) {
  PROFILE_ASAN_MAPPING();
  return kMidMemBeg && a >= kMidMemBeg && a <= kMidMemEnd;
}

static inline bool AddrIsInMidShadow(uptr a) {
  PROFILE_ASAN_MAPPING();
  return kMidMemBeg && a >= kMidShadowBeg && a <= kMidShadowEnd;
}

static inline bool AddrIsInHighMem(uptr a) {
  PROFILE_ASAN_MAPPING();
  return kHighMemBeg && a >= kHighMemBeg && a <= kHighMemEnd;
}

static inline bool AddrIsInHighShadow(uptr a) {
  PROFILE_ASAN_MAPPING();
  return kHighMemBeg && a >= kHighShadowBeg && a <= kHighShadowEnd;
}

static inline bool AddrIsInShadowGap(uptr a) {
  PROFILE_ASAN_MAPPING();
  if (kMidMemBeg) {
    if (a <= kShadowGapEnd)
      return ASAN_SHADOW_OFFSET == 0 || a >= kShadowGapBeg;
    return (a >= kShadowGap2Beg && a <= kShadowGap2End) ||
           (a >= kShadowGap3Beg && a <= kShadowGap3End);
  }
  // In zero-based shadow mode we treat addresses near zero as addresses
  // in shadow gap as well.
  if (ASAN_SHADOW_OFFSET == 0)
    return a <= kShadowGapEnd;
  return a >= kShadowGapBeg && a <= kShadowGapEnd;
}

// Must be after all calls to PROFILE_ASAN_MAPPING().
static const uptr kAsanMappingProfileSize = __LINE__;

}  // namespace __sanitizer

#endif  // ASAN_MAPPING_H
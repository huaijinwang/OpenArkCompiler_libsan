#ifndef SANITIZER_THREAD_H
#define SANITIZER_THREAD_H
#include "asan_internal_defs.h"
#include "pthread.h"
#include "sys/types.h"
#include "sys/syscall.h"
#include "sys/signal.h"
#include "unistd.h"

namespace __sanitizer {

inline Tid GetCurrentTid() {
  Tid tid;
  tid = syscall(SYS_gettid);
  return tid;
}


inline Pid GetCurrentPid() {
  Pid pid;
  pid = getpid();
  return pid;
}

// This merely works for Linux OS
inline bool IsMainThread() {
  return GetCurrentTid() == GetCurrentPid();
}


inline Tid GetCurrentTidOrInvalid() {
  return IsMainThread() ? kMainTid : kInvalidTid;
}

  
} // namespace __sanitizer 

#endif // SANITIZER_THREAD_H
#ifndef SANITIZER_LINUX_SYSCALL_H
#define SANITIZER_LINUX_SYSCALL_H
#include "fcntl.h"
#include "unistd.h"
#include "sys/syscall.h"
#include "sys/types.h"
#include "sys/signal.h"
#include "sys/mman.h"

namespace __sanitizer
{

INTERFACE_ATTRIBUTE uptr internal_openat(int dirfd, const char *pathname, int flags, int mode);
INTERFACE_ATTRIBUTE uptr internal_ftruncate(int fd, OFF_T length);
INTERFACE_ATTRIBUTE uptr internal_mmap(void *addr, uptr length, int prot, int flags, int fd, u64 offset);
INTERFACE_ATTRIBUTE uptr internal_unlink(const char* path);

} // namespace __sanitizer


#endif // SANITIZER_LINUX_SYSCALL_H
// do not use namespace __sanitizer;
#ifndef ASAN_INTERFACES_MACRO
#error "Define ASAN_INTERFACES_MACRO before include this file"
#else
// malloc and free
ASAN_INTERFACES_MACRO(void *, malloc, __sanitizer::uptr);
ASAN_INTERFACES_MACRO(void*, calloc, __sanitizer::uptr, __sanitizer::uptr);
ASAN_INTERFACES_MACRO(void*, realloc, void* ptr, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(void*, alloca, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(void, free, void *);

// intrinsic functions
ASAN_INTERFACES_MACRO(void*, memcpy, void *to, const void *from, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(void*, memset, void *block, int c, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(void*, memmove, void* to, const void* from, __sanitizer::uptr size);

// str* functions
ASAN_INTERFACES_MACRO(int, memcmp, const void *a1, const void *a2, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(__sanitizer::uptr, strlen, const char *s);
ASAN_INTERFACES_MACRO(__sanitizer::uptr, strnlen, const char *s, __sanitizer::uptr maxlen);
ASAN_INTERFACES_MACRO(char *, strcat, char *to, const char *from);
ASAN_INTERFACES_MACRO(char *, strncat, char *to, const char *from, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(char *, strcpy, char *to, const char *from);
ASAN_INTERFACES_MACRO(char *, strncpy, char *to, const char *from, __sanitizer::uptr size);
ASAN_INTERFACES_MACRO(char *, strdup, const char *s);

ASAN_INTERFACES_MACRO(wchar_t *, wcscpy, wchar_t *dest, const wchar_t *src);
ASAN_INTERFACES_MACRO(wchar_t *, wcsncpy, wchar_t *dest, const wchar_t *src, __sanitizer::uptr n);

// printf functions
ASAN_INTERFACES_MACRO(int, printf, const char *format, ...);
ASAN_INTERFACES_MACRO(int, snprintf, char * s, size_t n, const char * format, ...)

ASAN_INTERFACES_MACRO(const char*, strstr, const char *s1, const char *s2);
ASAN_INTERFACES_MACRO(const char*, strchr, const char *str, int c);

#endif // ASAN_INTERFACES_MACRO
#!/bin/bash

gcc_linaro_lib="$MAPLE_ROOT/tools/gcc-linaro-7.5.0/aarch64-linux-gnu/lib64"
qemu_linked_lib="$MAPLE_ROOT/tools/gcc-linaro-7.5.0/aarch64-linux-gnu/libc"
LD_PRELOAD="$gcc_linaro_lib/libasan.so"
qemu_path="$MAPLE_ROOT/output/tools/bin/qemu-aarch64"
bin_path=$1
# cmd="ASAN_OPTIONS=detect_leaks=0 $qemu_path -L $qemu_linked_lib -E LD_PRELOAD=$LD_PRELOAD -E LD_LIBRARY_PATH=$gcc_linaro_lib $bin_path"
# cmd="ASAN_OPTIONS=detect_leaks=0 $qemu_path -L $qemu_linked_lib -E LD_LIBRARY_PATH=$gcc_linaro_lib -g 1234 $bin_path"
cmd="ASAN_OPTIONS=detect_leaks=0 $qemu_path -L $qemu_linked_lib -E LD_LIBRARY_PATH=$gcc_linaro_lib $bin_path"

echo $cmd
eval $cmd


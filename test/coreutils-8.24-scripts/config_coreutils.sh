#!/bin/bash

cur=`pwd` # this is supposed to be the root of coreutils source dir
configure_path="$cur/configure"
if [[ -f $configure_path ]]
then
	echo "coreutils_src_root=$configure_path"
else
	echo "Please run in the coreutils root directory (where the file configure exists)"
	exit 1
fi

echo "MAPLE_ROOT=$MAPLE_ROOT"
BASEDIR=$(dirname "$0")
script_path=$0
CFLAGS="-fgnu89-inline -isystem $MAPLE_ROOT/tools/gcc-linaro-7.5.0/aarch64-linux-gnu/libc/usr/include -isystem $MAPLE_ROOT/tools/gcc-linaro-7.5.0/lib/gcc/aarch64-linux-gnu/7.5.0/include"
ARCH=aarch64-linux-gnu

cmd="./configure CC=$BASEDIR/arkcc_asan.py CFLAGS=\"$CFLAGS\" --host=$ARCH --target=$ARCH"

echo
echo "RUN CMD:" $cmd
eval $cmd
